Profe estoy practicando la parte de herencia multiple,la idea de como arme esto es la siguiente: 

Comienza con el archivo ## figurageometrica.py ## donde creo la clase Padre figurageometrica (con los atributos privados "alto","ancho")

Despues creo un segundo archivo # color.py # generando otra clase Padre Color (con un solo atributo privado)

El ejecicio pide que generar otro archivo # cuadrado.py # creando una clase que va a heredar los atributos de las clases padres FiguraGeometrica y Color 

y finalmente creo el ultimo archivo en donde creo el objeto y llamo a la clase Cuadrado....


¿Que es lo que sucede?

Cuando coloco como Privados a (ancho y alto) en la clase FiguraGeometrica (y en cada lado donde corresponde),cuando llamo al metodo area dentro de mi archivo testfigurageometrica.py, me sale un error **AttributeError: 'Cuadrado' object has no attribute '_Cuadrado__alto'**.
Lo raro es que no me sucede lo mismo con mi atributo color que estando como PRIVADO funciona sin problemas-

Tambien probe comentando los metodos get y set de alto y ancho , luego volvi a colocarlos como PUBLICOS a los atributos alto y ancho , dejando como PRIVADO solo a Color ,cuando realizo la llamda funciona sin problemas
