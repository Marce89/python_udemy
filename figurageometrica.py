# abc = ABSTRACT BASE CLASS
from abc import ABC , abstractmethod


class FiguraGeometrica(ABC):
    
    def __init__(self, ancho , alto):
        self.__ancho = ancho
        self.__alto = alto             #Ambos estan como atributos PUBLICOS
    
    ''' Al crear metodos abstractos en mi clase PADRE , cuando este generando las clases HIJAS que hereden deL PADRE, python solicitara que implementemos,para esa nueva clase (hija),el metodo abstracto definido en el padre (EJ: area)'''
    
    @abstractmethod       #al crear este Metodo abstracto significa que solo podre crear objectos desde los metodos de las clases HIJAS 
    def area(self):
        pass
    
    def get__ancho(self):
        return self.__ancho
    def set__ancho(self, parametro):
        self.__ancho = parametro

    
    def get__alto(self):
        return self.__alto
    def set_alto(self, parametro1):
        self.__alto = parametro1
        
    def __str__(self):
        return " Ancho:" + str(self.__ancho) + "  Alto:" + str(self.__alto)