
from cuadrado import Cuadrado
#from cuadrado import Rectangulo
#from cuadrado import Triangulo
from figurageometrica import FiguraGeometrica

info_cuadrado = Cuadrado(6,'Verde')
#info_rectangulo = Rectangulo(3,6,'Azul')
#info_triangulo = Triangulo(10,2,'Marron')



#print(info_cuadrado)
#print(info_rectangulo)
#print(info_triangulo)
#Method Resolution Order 
#print(Cuadrado.mro())


#No es posible crear objetos de una clase abstracta(ver class FiguraGeometrica)
#fig=FiguraGeometrica()


print(info_cuadrado.area())
print(info_cuadrado.get_color())