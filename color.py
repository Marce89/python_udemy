class Color:
    def __init__(self, color):
        self.__color = color
        
    def get_color(self):
        return self.__color
    def set_color(self, parametro2):
        self.__color = parametro2         # atributo PRIVADO
    def __str__(self):
        return "  Color:" + self.__color
        
""" objeto2 = Color("rojo")
print(objeto2.get_color())

objeto2.set_color("violeta")
print(objeto2.get_color()) """