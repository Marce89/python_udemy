from figurageometrica import FiguraGeometrica
from color import Color



class Cuadrado(FiguraGeometrica , Color):
    def __init__(self, lado, color) :
        FiguraGeometrica.__init__(self , lado, lado)
        Color.__init__(self, color)
    
    def area (self) :
        return self.__alto * self.__ancho
    
    def __str__(self) :
        return 'Datos del Cuadrado ='+FiguraGeometrica.__str__(self)+Color.__str__(self)+'  Area:'+ str(Cuadrado.area(self))

""" class Rectangulo(FiguraGeometrica , Color):
    def __init__(self, alto, ancho, color):
        FiguraGeometrica.__init__(self,ancho,alto)
        Color.__init__(self, color)
    
    def area (self) :
        return self.alto * self.ancho
    
    def __str__(self) :
        return 'Datos del Rectangulo ='+FiguraGeometrica.__str__(self)+Color.__str__(self)+'  Area:'+ str(Cuadrado.area(self))
    


class Triangulo(FiguraGeometrica , Color):
    def __init__(self, alto, ancho, color):
        FiguraGeometrica.__init__(self,ancho,alto)
        Color.__init__(self, color)
    
    def area (self) :
        return (self.alto * self.ancho)/2
    
    def __str__(self) :
        return 'Datos del Triangulo ='+FiguraGeometrica.__str__(self)+Color.__str__(self)+'  Area:'+ str(Cuadrado.area(self)) """
        